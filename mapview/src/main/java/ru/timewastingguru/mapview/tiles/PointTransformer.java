package ru.timewastingguru.mapview.tiles;

import android.util.Pair;

public class PointTransformer {

    private int viewPortWidth;
    private int viewPortHeigh;

    private final int mapWidth;
    private final int mapHeight;

    private final int tileWidth;
    private final int tileHeight;

    private int viewPortX;
    private int viewPortY;

    public PointTransformer(TilesProvider tilesProvider) {
        Pair<Integer, Integer> mapSize = tilesProvider.getMapSize();
        this.mapWidth = mapSize.first;
        this.mapHeight = mapSize.second;

        Pair<Integer, Integer> tileSize = tilesProvider.getTileSize();
        this.tileWidth = tileSize.first;
        this.tileHeight = tileSize.second;
    }

    public int[] onMeasured(int width, int height){
        this.viewPortWidth = width;
        this.viewPortHeigh = height;
        viewPortX = ((int) Math.ceil((float) width / tileWidth)) + 1;
        viewPortY = ((int) Math.ceil((float) height / tileHeight)) + 1;

        return getViewPortVisibleIndexes(0, 0); // for default coordinates
    }

    private int[] getViewPortVisibleIndexes(float xOffset, float yOffset) {
        int[] result = new int[viewPortX * viewPortY];

        for (int row = 0; row < viewPortY; row++) {

            for (int col = 0; col < viewPortX; col++) {

                int mapIndexX = (int) (xOffset / tileWidth + col);
                int mapIndexY = (int) (yOffset / tileHeight + row);
                result[row * viewPortX + col] = mapIndexY * mapWidth + mapIndexX; // calculate map index for view port index :D

            }

        }

        return result;
    }


    public int[] fromReal(float x, float y) {
       return getViewPortVisibleIndexes(x, y);
    }

    public Pair<Integer, Integer> toReal(int tileIndex) {
        return null;
    }

}
