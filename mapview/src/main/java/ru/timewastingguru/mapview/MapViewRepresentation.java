package ru.timewastingguru.mapview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Scroller;

import ru.timewastingguru.mapview.tiles.cache.FileCacheTilesClient;
import ru.timewastingguru.mapview.tiles.clap.ClapTilesProvider;
import ru.timewastingguru.mapview.tiles.network.UrlConnectionTilesClient;
import ru.timewastingguru.mapview.tiles.openstreetmap.OpenStreetMapTilesProvider;

/**
 * Created by a.tkachenko on 24.02.17.
 */

public class MapViewRepresentation extends View implements GestureDetector.OnGestureListener {

    private GestureDetector gestureDetector;
    private Paint paint;
    private float translateX;
    private float translateY;
    private Paint bitmapPaint;
    private MapProvider tilesProvider;
    private MapViewPort tiles;
    private Scroller scroller;

    public MapViewRepresentation(Context context) {
        super(context);
        init(context, null, 0);
    }

    public MapViewRepresentation(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public MapViewRepresentation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MapViewRepresentation(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        scroller = new Scroller(context);
        gestureDetector = new GestureDetector(context, this);
        paint = new Paint();
        paint.setColor(Color.RED);
        bitmapPaint = new Paint();
        bitmapPaint.setFilterBitmap(true);

        tilesProvider = new MapProvider(context, new OpenStreetMapTilesProvider(new UrlConnectionTilesClient(), new FileCacheTilesClient(context)));

        tiles = tilesProvider.getViewPort();

        tiles.setContentChangeListener(new MapViewPort.ContentChangeListener() {
            @Override
            public void onContentChanged() {
                invalidate();
            }
        });

        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getViewTreeObserver().removeOnGlobalLayoutListener(this);
                tilesProvider.onMeasure(getWidth(), getHeight());
            }
        });

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!scroller.isFinished() && scroller.computeScrollOffset()) {
            if (scroller.getCurrX() > -(border - getWidth()) && scroller.getCurrX() < 0) {
                translateX = (float) scroller.getCurrX();
            }

            if (scroller.getCurrY() > -(border - getHeight()) && scroller.getCurrY() < 0) {
                translateY = (float) scroller.getCurrY();
            }
            tilesProvider.onScroll(-this.translateX, -this.translateY);

//            System.out.println(String.format("onDraw x = %s, y = %s ", scroller.getCurrX(), scroller.getCurrY()));

            invalidate();
        }
        canvas.save();
        canvas.translate(translateX, translateY);



        for (int i = 0; i < tiles.getTiles().size(); i++) {
            Tile tile = tiles.getTiles().valueAt(i);
            Bitmap bitmap = tile.bitmap;
            int left = bitmap.getWidth() * tile.mapX;
            int top = bitmap.getHeight() * tile.mapY;
            canvas.drawBitmap(bitmap, left, top, bitmapPaint);
        }

        canvas.restore();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        if (!scroller.isFinished()) {
            scroller.abortAnimation();
        }
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public static final int border = ClapTilesProvider.TILE_SIZE * ClapTilesProvider.MAP_X_SIZE;

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        this.translateX -= distanceX;
        this.translateY -= distanceY;

        if (this.translateX > 0) this.translateX = 0;
        if (this.translateY > 0) this.translateY = 0;

        if (this.translateX - getWidth() < -border) this.translateX = -border + getWidth();
        if (this.translateY - getHeight() < -border) this.translateY = -border + getHeight();

//        System.out.println(String.format("onScroll x = %s, y = %s,  dx = %s, dy = %s",  this.translateX, this.translateY, distanceX, distanceY));

        tilesProvider.onScroll(-this.translateX, -this.translateY);
        invalidate();
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
//        System.out.println(String.format("onFling velocityX = %s, velocityY = %s", velocityX, velocityY));
        scroller.fling(Math.round(translateX), Math.round(translateY), Math.round(velocityX), Math.round(velocityY), -border, 0, -border, 0);
        postInvalidate();
        return true;
    }


}
