package ru.timewastingguru.mapview;

import android.util.SparseArray;


/**
 * Created by a.tkachenko on 24.02.17.
 */

public class MapViewPort {

    private SparseArray<Tile> tiles = new SparseArray<>();

    private ContentChangeListener contentChangeListener;

    public void addTile(int index, Tile tile){
        tiles.put(index, tile);
        notifyListener();
    }

    public void remove(int index){
        tiles.remove(index);
    }

    private void notifyListener() {
        if (contentChangeListener != null) {
            contentChangeListener.onContentChanged();
        }
    }

    public SparseArray<Tile> getTiles(){
        return tiles;
    }

    public void setContentChangeListener(ContentChangeListener contentChangeListener) {
        this.contentChangeListener = contentChangeListener;
    }

    public interface ContentChangeListener {
        void onContentChanged();
    }

}
