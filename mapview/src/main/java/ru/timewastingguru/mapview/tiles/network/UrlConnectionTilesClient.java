package ru.timewastingguru.mapview.tiles.network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UrlConnectionTilesClient implements RemoteTilesClient {
    @Override
    public Bitmap loadBitmap(String url) {
        return getBitmapFromURL(url);
    }

    public static Bitmap getBitmapFromURL(String src) {
        Bitmap result = null;
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            result = BitmapFactory.decodeStream(input);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
