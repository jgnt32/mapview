package ru.timewastingguru.mapview;

import android.content.Context;
import android.util.SparseArray;
import android.util.SparseIntArray;

import ru.timewastingguru.mapview.tiles.PointTransformer;
import ru.timewastingguru.mapview.tiles.TilesProvider;

/**
 * Created by a.tkachenko on 24.02.17.
 */

public class MapProvider implements TilesProvider.TileLoadListener {

    private final Context context;

    private final MapViewPort viewPort = new MapViewPort();


    private PointTransformer pointTransformer ;

    private SparseIntArray inVisibleZone = new SparseIntArray();
    private SparseIntArray outOfVisibleZone = new SparseIntArray();

    private final TilesProvider tilesProvider;


    public MapProvider(final Context context, TilesProvider tilesProvider) {
        this.context = context;
        this.tilesProvider = tilesProvider;
        tilesProvider.setListener(this);
        pointTransformer = new PointTransformer(this.tilesProvider);
    }

    public MapViewPort getViewPort() {
        return viewPort;
    }

    public void onScroll(float x, float y){
        int[] indexes = pointTransformer.fromReal(x, y);
        inVisibleZone.clear();
        outOfVisibleZone.clear();
        SparseArray<Tile> tiles = viewPort.getTiles();


        for (int i = 0; i < tiles.size(); i++) {
            int key = tiles.keyAt(i);
            outOfVisibleZone.put(key, key);
        }

        for (int index : indexes) {

            if (tiles.indexOfKey(index) < 0) {
                inVisibleZone.put(index, index);
            }

            outOfVisibleZone.delete(index);

        }

        for (int i = 0; i < inVisibleZone.size(); i++) {
            tilesProvider.requestTile(inVisibleZone.valueAt(i));
        }

        for (int i = 0; i < outOfVisibleZone.size(); i++) {
            int key = outOfVisibleZone.keyAt(i);
            tiles.get(key).bitmap.recycle();
            tiles.delete(key);
        }

    }

    @Override
    public void onTileDownLoaded(int index, Tile tileResult) {
        viewPort.addTile(index, tileResult);
    }

    public void onMeasure(int width, int height){
        int[] ints = pointTransformer.onMeasured(width, height);
        for (int index : ints) {
            tilesProvider.requestTile(index);
        }

    }

}
