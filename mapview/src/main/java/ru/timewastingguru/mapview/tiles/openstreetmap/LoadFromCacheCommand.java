package ru.timewastingguru.mapview.tiles.openstreetmap;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;

import ru.timewastingguru.mapview.Tile;
import ru.timewastingguru.mapview.tiles.TilesProvider;
import ru.timewastingguru.mapview.tiles.cache.CacheTilesClient;
import ru.timewastingguru.mapview.tiles.network.RemoteTilesClient;

public class LoadFromCacheCommand extends TilesCommand {

    public LoadFromCacheCommand(RemoteTilesClient remoteTilesClient, CacheTilesClient cacheTilesClient, String tilesUrl, Handler handler, int index, TilesProvider.TileLoadListener listener) {
        super(remoteTilesClient, cacheTilesClient, tilesUrl, handler, index, listener);
    }

    @Override
    public void run() {
        Bitmap finalBitmap = cacheTilesClient.loadBitmap(index+".png");

        if (finalBitmap == null) {
            finalBitmap = createClap("Loading...", 30, Color.GRAY);
        }
        final Bitmap finalBitmap1 = finalBitmap;
        handler.post(new Runnable() {
            @Override
            public void run() {
                listener.onTileDownLoaded(index, new Tile(index % OpenStreetMapTilesProvider.MAP_Y_SIZE, index / OpenStreetMapTilesProvider.MAP_Y_SIZE, finalBitmap1));
            }
        });
    }
}
