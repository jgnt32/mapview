package ru.timewastingguru.mapview.tiles.cache;


import android.graphics.Bitmap;

public interface CacheTilesClient {

    Bitmap loadBitmap(String url);

    void save(Bitmap bitmap, String url);

    boolean isExist(String urls);

}
