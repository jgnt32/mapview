package ru.timewastingguru.mapview.tiles.network;


import android.graphics.Bitmap;

public interface RemoteTilesClient {

    Bitmap loadBitmap(String url);

}
