package ru.timewastingguru.mapview.tiles;

import android.util.Pair;

import ru.timewastingguru.mapview.Tile;

public abstract class TilesProvider {

    protected  TileLoadListener listener;

    public TileLoadListener getListener() {
        return listener;
    }

    public void setListener(TileLoadListener listener) {
        this.listener = listener;
    }

    public abstract void requestTile(int index);

    public abstract Pair<Integer, Integer> getMapSize();

    public abstract Pair<Integer, Integer> getTileSize();

    public interface TileLoadListener {
        void onTileDownLoaded(int tileIndex, Tile tile);
    }


}
