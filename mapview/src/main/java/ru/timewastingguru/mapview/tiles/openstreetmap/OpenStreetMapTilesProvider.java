package ru.timewastingguru.mapview.tiles.openstreetmap;

import android.os.Handler;
import android.util.Pair;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import ru.timewastingguru.mapview.tiles.TilesProvider;
import ru.timewastingguru.mapview.tiles.cache.CacheTilesClient;
import ru.timewastingguru.mapview.tiles.network.RemoteTilesClient;

public class OpenStreetMapTilesProvider extends TilesProvider {

    public static final int TILE_SIZE = 256;

    public static final int MAP_X_SIZE = 100;

    public static final int MAP_Y_SIZE = 100;

    private final ExecutorService executor =  new ThreadPoolExecutor(3, 5, 2, TimeUnit.SECONDS,
            new LinkedBlockingQueue<Runnable>());

    private final ExecutorService localExecutor =  new ThreadPoolExecutor(3, 5, 2, TimeUnit.SECONDS,
            new LinkedBlockingQueue<Runnable>());


    private final RemoteTilesClient remoteTilesClient;
    private final CacheTilesClient cacheTilesClient;

    public OpenStreetMapTilesProvider(RemoteTilesClient remoteTilesClient,
                                      CacheTilesClient cacheTilesClient) {
        this.remoteTilesClient = remoteTilesClient;
        this.cacheTilesClient = cacheTilesClient;
    }

    Handler handler = new Handler();

    @Override
    public void requestTile(final int index) {
        int x = index % MAP_X_SIZE;
        int y = index / MAP_Y_SIZE;

        final String tilesUrl = String.format("http://a.tile.openstreetmap.org/8/%d/%d.png", x, y);
        executor.execute(new LoadFromNetworkIfNeed(remoteTilesClient, cacheTilesClient, tilesUrl, handler, index, listener));
        localExecutor.execute(new LoadFromCacheCommand(remoteTilesClient, cacheTilesClient, tilesUrl, handler, index, listener));
    }

    @Override
    public Pair<Integer, Integer> getMapSize() {
        return new Pair<>(MAP_X_SIZE, MAP_Y_SIZE);
    }

    @Override
    public Pair<Integer, Integer> getTileSize() {
        return new Pair<>(TILE_SIZE, TILE_SIZE);
    }

}
