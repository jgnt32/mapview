package ru.timewastingguru.mapview.tiles.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileOutputStream;

public class FileCacheTilesClient implements CacheTilesClient {

    private final Context context;

    public FileCacheTilesClient(Context context) {
        this.context = context;
    }

    @Override
    public Bitmap loadBitmap(String url) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        String pathname = context.getCacheDir().getPath() + "/" + url;

        File file = new File(pathname);
        if (!file.exists()) {
            return null;
        } else {
            return BitmapFactory.decodeFile(pathname, options);
        }
    }

    @Override
    public void save(Bitmap bitmap, String url) {

        String pathname = context.getCacheDir() + "/" + url;
        File pictureFile = new File(pathname);

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isExist(String url) {
        String pathname = context.getCacheDir().getPath() + "/" + url;
        return new File(pathname).exists();
    }

}
