package ru.timewastingguru.mapview.tiles.openstreetmap;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;

import ru.timewastingguru.mapview.tiles.TilesProvider;
import ru.timewastingguru.mapview.tiles.cache.CacheTilesClient;
import ru.timewastingguru.mapview.tiles.network.RemoteTilesClient;

public abstract class TilesCommand implements Runnable {

    protected final RemoteTilesClient remoteTilesClient;
    protected final CacheTilesClient cacheTilesClient;
    protected final String tilesUrl;
    protected Handler handler;
    protected final int index;
    protected final TilesProvider.TileLoadListener listener;

    public TilesCommand(RemoteTilesClient remoteTilesClient, CacheTilesClient cacheTilesClient,
                         String tilesUrl, Handler handler, int index, TilesProvider.TileLoadListener listener) {
        this.remoteTilesClient = remoteTilesClient;
        this.cacheTilesClient = cacheTilesClient;
        this.tilesUrl = tilesUrl;
        this.handler = handler;
        this.index = index;
        this.listener = listener;
    }


    protected Bitmap createClap(String text, float textSize, int textColor) {
        Paint paint = new Paint();
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        Bitmap image = Bitmap.createBitmap(OpenStreetMapTilesProvider.TILE_SIZE, OpenStreetMapTilesProvider.TILE_SIZE, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);

        canvas.drawText(text, OpenStreetMapTilesProvider.TILE_SIZE / 4 , OpenStreetMapTilesProvider.TILE_SIZE / 2, paint); // divide by 4 because too lazy for calculating text width
        return image;
    }

}
