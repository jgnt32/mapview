package ru.timewastingguru.mapview.tiles.clap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Pair;

import ru.timewastingguru.mapview.Tile;
import ru.timewastingguru.mapview.tiles.TilesProvider;

/**
 * Created by a.tkachenko on 24.02.17.
 */

public class ClapTilesProvider extends TilesProvider {

    public static final int TILE_SIZE = 256;

    public static final int MAP_X_SIZE = 100;

    public static final int MAP_Y_SIZE = 100;

    private final Context context;

    public ClapTilesProvider(Context context) {
        this.context = context;
    }

    @Override
    public void requestTile(int index) {
        Bitmap bitmap = textAsBitmap("" + index, 30, Color.RED);
        listener.onTileDownLoaded(index, new Tile(index % MAP_Y_SIZE, index / MAP_Y_SIZE, bitmap));
    }

    @Override
    public Pair<Integer, Integer> getMapSize() {
        return new Pair<>(MAP_X_SIZE, MAP_Y_SIZE);
    }

    @Override
    public Pair<Integer, Integer> getTileSize() {
        return new Pair<>(TILE_SIZE, TILE_SIZE);
    }


    public Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint();
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        Bitmap image = Bitmap.createBitmap(TILE_SIZE, TILE_SIZE, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);

        canvas.drawText(text, TILE_SIZE / 2, TILE_SIZE / 2, paint);

        canvas.drawLine(0, 0, 0, TILE_SIZE, paint);
        canvas.drawLine(0, 0, TILE_SIZE, 0, paint);

        canvas.drawLine(TILE_SIZE, 0, TILE_SIZE, TILE_SIZE, paint);
        canvas.drawLine(0, TILE_SIZE, TILE_SIZE, TILE_SIZE, paint);

        return image;
    }
}
