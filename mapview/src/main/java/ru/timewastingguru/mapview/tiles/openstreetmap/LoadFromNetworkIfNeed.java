package ru.timewastingguru.mapview.tiles.openstreetmap;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;

import ru.timewastingguru.mapview.Tile;
import ru.timewastingguru.mapview.tiles.TilesProvider;
import ru.timewastingguru.mapview.tiles.cache.CacheTilesClient;
import ru.timewastingguru.mapview.tiles.network.RemoteTilesClient;

public class LoadFromNetworkIfNeed extends TilesCommand {

    public LoadFromNetworkIfNeed(RemoteTilesClient remoteTilesClient, CacheTilesClient cacheTilesClient, String tilesUrl, Handler handler, int index, TilesProvider.TileLoadListener listener) {
        super(remoteTilesClient, cacheTilesClient, tilesUrl, handler, index, listener);
    }

    @Override
    public void run() {
        String url = index + ".png";

        if (!cacheTilesClient.isExist(url)) {

            Bitmap bitmap = remoteTilesClient.loadBitmap(tilesUrl);
            if (bitmap == null) {
                bitmap = createClap("¯\\_(ツ)_/¯", 30, Color.CYAN);
            } else {
                cacheTilesClient.save(bitmap, url);
            }
            final Bitmap finalBitmap = bitmap;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    listener.onTileDownLoaded(index, new Tile(index % OpenStreetMapTilesProvider.MAP_Y_SIZE, index / OpenStreetMapTilesProvider.MAP_Y_SIZE, finalBitmap));
                }
            });

        }

    }
}
