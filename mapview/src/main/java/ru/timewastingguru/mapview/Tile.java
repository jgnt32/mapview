package ru.timewastingguru.mapview;

import android.graphics.Bitmap;

public class Tile {
    final int mapX;
    final int mapY;

    final Bitmap bitmap;

    public Tile(int mapX, int mapY, Bitmap bitmap) {
        this.mapX = mapX;
        this.mapY = mapY;
        this.bitmap = bitmap;
    }
}